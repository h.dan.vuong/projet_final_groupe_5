#ifndef ENNEMI_H
#define ENNEMI_H

#include <string>
#include "Personnage.h"

class Ennemi: public Personnage
{
    private:

    int puissance;
    int discussion;

    public:

    Ennemi();
    Ennemi(int, int);
    void infos();
    void setPuissance(int);
    void setDiscussion(int);
    int getPuissance();
    int getDiscussion();

};


#endif