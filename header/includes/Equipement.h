#ifndef Equipement_H
#define Equipement_H

#include <string>
#include <vector>


class Equipement
{
    private:
    std::string name;
    std::vector<int> attaque;

    public:
    std ::string getname(){return this->name;}
    void Setname(std::string name){this->name=name;}
    int rollAttaque();

    Equipement();
    Equipement(std::string, std::string);

};

#endif
