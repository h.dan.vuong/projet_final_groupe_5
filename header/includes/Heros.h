#ifndef HEROS_H
#define HEROS_H
#include <string>
#include "Personnage.h"
#include "Inventaire.h"
#include <iostream>
#include "Arme.h"

class Heros: public Personnage
{
    private:
    int force;
    int intelligence;
    int ruse;
    int dexterite;
    int constitution;
    int endurance;
    int point=100;
    int leftpoints;
    Inventaire inventaire;

    public:

    Heros();
    Heros(string,int,int,int,int,int,int,int);
    virtual ~Heros();
    void affiche();
    void AddObjet(int numObjet);
    int CheckObjet(int numObjet);

    int Getforce(){return force;}
    int Getintelligence(){return intelligence;}
    int Getruse(){return ruse;}
    int Getdexterite(){return dexterite;}
    int Getconstitution(){return constitution;}
    int Getendurance(){return endurance;}
    int GetLeftPoints(){return leftpoints;}
    void SetInventaire(Inventaire inventaire){this->inventaire=inventaire;}
    void Setforce(int value_force){

      if(value_force <= leftpoints){
           force = value_force;
           this->leftpoints = leftpoints - force;

       }

    }

    void Setintelligence(int value_intelligence){
      if(value_intelligence <= leftpoints){
           intelligence = value_intelligence;
           this->leftpoints = leftpoints - intelligence;
       }

    }

    void Setruse(int value_ruse){
      if(value_ruse <= leftpoints){
           ruse = value_ruse;
           this->leftpoints = leftpoints - ruse;
       }

    }
   void Setdexterite(int value_dexterite){
      if(value_dexterite <= leftpoints){
           dexterite = value_dexterite;
           this->leftpoints = leftpoints - dexterite;
       }

    }
   void Setconstitution(int value_constitution){
      if(value_constitution <= leftpoints){
           constitution = value_constitution;
           this->leftpoints = leftpoints - constitution;
       }

    }
    void Setendurance(int value_endurance){
      if(value_endurance <= leftpoints){
           endurance = value_endurance;
           this->leftpoints = leftpoints - endurance;
       }

    }

void loop();
void Equiper(std::string filepath);
void afficherEquipement();
void equipement(Equipement e);
std::vector<Equipement> getEquipement();
};

#endif
