#ifndef Inventaire_H
#define Inventaire_H
#define NB 10
#include <string>
#include "Inventaire.h"
#include "Objet.h"
#include <vector>
#include "Equipement.h"
class Inventaire
{
    private:

    int nbrObjets=0;
    Objet *mesObjets[NB];
    std::vector<Equipement> mesEquipements;
    int nbrEquipements;

    public:

    Inventaire();
    virtual ~Inventaire();
    int GetnbrObjet(){return nbrObjets;}
    void SetnbrObjet(int val){nbrObjets=val;}
    void getObjet();
    void infos();
    int CheckObjet(int numObjet);
    void setObjets(int numObjet);

    void ajouterEquipement(Equipement);
    std::vector<Equipement> getEquipements(){return this->mesEquipements;}
    void getarmefromfile(std::string filepath);//chemin vers le fichier

};

#endif

