#ifndef NARRATION_H
#define NARRATION_H

#include "../../header/includes/Heros.h"
#include "../../header/includes/Ennemi.h"

class Narration
{
    private:
    Heros *heros;
    Ennemi *ennemi;

    public:

    Narration();
    Narration(Heros *heros);
    void setEnnemi(Ennemi* ennemi);
    Heros getHeros();
    void GameCore();

};

#endif