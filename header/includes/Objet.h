#ifndef Objet_H
#define Objet_H

#include <iostream>
#include <string>
using namespace std;


class Objet
{
    private:
    int  numeroDeLobjet;


    public:
    Objet();
    Objet(int);
    int getnumeroDeLobjet(){ return numeroDeLobjet; }
    void infos();


};

#endif
