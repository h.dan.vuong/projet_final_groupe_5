#include <iostream>
#include "../header/includes/Personnage.h"
#include "../header/includes/Heros.h"
#include "../header/includes/Narration.h"
#include "../header/includes/Equipement.h"
#include <string>
using namespace std;
int pv;
int main()
{

  Heros *heros = new Heros();
  //heros->loop();

  Equipement e("epee", "1 5 10 2");
  heros->equipement(e);

  Narration narration(heros);
  narration.GameCore();

  return 0;
}