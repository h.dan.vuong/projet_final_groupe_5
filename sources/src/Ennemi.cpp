#include "../../header/includes/Ennemi.h"
#include <iostream>

Ennemi::Ennemi()
{
    puissance = 0;
    discussion = 0;
}


Ennemi::Ennemi(int p_puissance,int p_discussion)
{
    puissance = p_puissance;
    discussion = p_discussion;
    std::cout << "Creation Ennemi" << std::endl;
}

void Ennemi::infos()
{
    std::cout << "Ennemi :" << std::endl;
    std::cout << "  Puissance : " << puissance << std::endl;
    std::cout << "  Discussion : " << discussion << std::endl;
}

int Ennemi::getDiscussion()
{
    return discussion;
}

int Ennemi::getPuissance()
{
    return puissance;
}

void Ennemi::setDiscussion(int p_discussion)
{
    discussion = p_discussion;
}

void Ennemi::setPuissance(int p_puissance)
{
    puissance = p_puissance;
}