#include "../../header/includes/Heros.h"

#include <iostream>
#include <string>
using namespace std;
Heros::Heros(): Personnage()//Appel au constructeur de la class mere
{
   // cout<<endl<<"ctor Heros()"<<endl;
    this->leftpoints = this->point;
    this->Setforce(0);
    this->Setintelligence(0);
    this->Setruse(0);
    this->Setconstitution(0);
    this->Setendurance(0);
    this->Setdexterite(0);


    //ctor
}
Heros::Heros(string n, int pv,int f,int i,int r,int d,int c,int e):Personnage(n,pv)
{
   Setforce(f);
   Setintelligence(i);
   Setruse(r);
   Setdexterite(d);
   Setconstitution(c);
   Setendurance(e);
   //cout<<endl<<"ctor Heros(n a,f,f,i,r,d,c,e)"<<endl;
}
Heros::~Heros()
{
//dtor
}

void Heros::affiche()
{
    Personnage::affiche();
    cout<<endl<<"force :"<<Getforce();
    cout<<endl<<"intel :"<<Getintelligence();
    cout<<endl<<"ruse :"<<Getruse();
    cout<<endl<<"dex :"<<Getdexterite();
    cout<<endl<<"endu :"<<Getendurance();
    cout<<endl<<"cons :"<<Getconstitution();
}

void Heros::loop()
{
int pv;
string name;
int f;
int e;
int d;
int r;
int c;
int i;

    std::cout << "choisie le pseudo de ton personnage :"<<std::endl;
    std::cin >> name;
    this->Setname(name);
    std::cout << "tu as un nombre de point de caracteristique de 100 a partager entre intelligence,force, dexterite,endurance et ruse:"<<std::endl;
    std::cout << "Force:"<<std::endl;
    std::cin >> f;
   while (f > GetLeftPoints())
    {

       std::cout << "La valeur que vous avez entrée est superieure à vos points (vos points : " << GetLeftPoints() << ")" << std::endl;
       std::cout << "Force:"<<std::endl;
       std::cin >> f;
    }
    this->Setforce(f);
    std::cout << "intelligence sachant qu'il vous reste:" << GetLeftPoints() <<std::endl;

    std::cin >> i;
     while (i > GetLeftPoints())
    {
      // std::cout << "La valeur que vous avez entrée est superieure à vos points" << myHero.GetLeftPoints << std::endl;

       std::cout << "intelligence:"<<std::endl;
       std::cin >> i;
    }
     this->Setintelligence(i);
     std::cout << "ruse sachant qu'il vous reste:" << GetLeftPoints() <<std::endl;
    std::cout << "ruse:"<<std::endl;
    std::cin >> r;
     while (r > GetLeftPoints())
    {
      // std::cout << "La valeur que vous avez entrée est superieure à vos points" << myHero.GetLeftPoints << std::endl;
       std::cout << "Ruse:"<<std::endl;
       std::cin >> r;
    }
     this->Setruse(r);
     std::cout << "dexterite sachant qu'il vous reste:" << GetLeftPoints() <<std::endl;
    std::cout << "dexterite:"<<std::endl;
    std::cin >> d;
     while (d > GetLeftPoints())
    {
      // std::cout << "La valeur que vous avez entrée est superieure à vos points" << myHero.GetLeftPoints << std::endl;

       std::cout << "dexterite:"<<std::endl;
       std::cin >> d;
    }
     this->Setdexterite(d);
     std::cout << "constitution sachant qu'il vous reste:" << GetLeftPoints() <<std::endl;
    std::cout << "constitution:"<<std::endl;
    std::cin >> c;
     while (c > GetLeftPoints())
    {
      // std::cout << "La valeur que vous avez entrée est superieure à vos points" << myHero.GetLeftPoints << std::endl;
       std::cout << "constitution sachant qu'il vous reste:"<<std::endl;
       std::cin >> c;
    }
     this->Setconstitution(c);
     std::cout << "endurance sachant qu'il vous reste:" << GetLeftPoints() <<std::endl;
    std::cout << "endurance:"<<std::endl;
    std::cin >> e;
     while (e > GetLeftPoints())
    {
      // std::cout << "La valeur que vous avez entrée est superieure à vos points" << myHero.GetLeftPoints << std::endl;
       std::cout << "endurance:"<<std::endl;
       std::cin >> e;
    }
   

}

void Heros::AddObjet(int p_numObjet)
{
   inventaire.setObjets(p_numObjet);
}

int Heros::CheckObjet(int p_numObjet)
{
   if (inventaire.CheckObjet(p_numObjet))
   {
      return 1;
   }
   else
   {
      return 0;
   }
}

void Heros::Equiper(std::string filepath){

    //this->inventaire.ajouterEquipement(arme);
    this->inventaire.getarmefromfile(filepath);
    }
void Heros::afficherEquipement(){
    std::cout<<"mes equipement :"<<std::endl;
for(Equipement e:this->inventaire.getEquipements())
{
    std::cout<<e.getname()<<std::endl;
}



}

void Heros::equipement(Equipement e)
{
   inventaire.ajouterEquipement(e);
}

std::vector<Equipement> Heros::getEquipement()
{
   return inventaire.getEquipements();
}