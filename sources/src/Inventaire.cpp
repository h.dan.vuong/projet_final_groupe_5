#include "../../header/includes/Inventaire.h"
#include "../../header/includes/Arme.h"
#include <iostream>
#include <fstream>
Inventaire::Inventaire()
{
    std::cout << "Creation Inventaire" << std::endl;
}

void Inventaire::getObjet()
{
    cout << endl << "Nb Objets : " << nbrObjets << endl;
    for(int i=0; i < nbrObjets; i++)
    {
        mesObjets[i]->infos();
    }
}

void Inventaire::setObjets(int numObjet)
{
    Objet* objet = new Objet(numObjet);
    if (nbrObjets < NB)
    {
        mesObjets[nbrObjets] = objet;

        cout << "Objet : " << mesObjets[nbrObjets]->getnumeroDeLobjet() << " a ete ajoute !" <<endl;
        nbrObjets++;
    }
    else
    {
         cout <<  endl << "Inventaire Plein !" << endl;
    }
}

int Inventaire::CheckObjet(int p_numObjet)
{
    if (nbrObjets <= 0)
    {
        return 0;
    }
    for  (Objet *obj : mesObjets)
    {
        if (obj->getnumeroDeLobjet() == p_numObjet)
        {
            return 1;
        }
    }
    return 0;
}

void Inventaire::infos()
{

}

Inventaire::~Inventaire()
{
}
       
void Inventaire::ajouterEquipement(Equipement e)
{
    mesEquipements.push_back(e);
}

void Inventaire::getarmefromfile(string filepath)
{
    string line;
    ifstream armefile (filepath);
    if (armefile.is_open())
    {
        while (getline(armefile,line))
        {

            cout<<line<<endl;
            string armename=line.substr(0,line.find(" "));
            Arme arme;
            arme.Setname(armename);
            ajouterEquipement(arme);
        }
        armefile.close();
    }
}

