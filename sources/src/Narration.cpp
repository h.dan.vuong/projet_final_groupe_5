#include "../../header/includes/Narration.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>


Narration::Narration()
{
    std::cout << "Creation Narration" << std::endl;
}

Narration::Narration(Heros *p_heros)
{
    heros = p_heros;
    std::cout << "Creation Narration" << std::endl;
}

Heros Narration::getHeros()
{
    return *heros;
}

int wordCount(std::string ligne)
{
    int nbr_mots = 0;
    for (char c : ligne)
    {
        if (c == ' ')
        {
            nbr_mots++;
        }
    }
    nbr_mots++;

    return nbr_mots;
}

void Narration::setEnnemi(Ennemi* p_ennemi)
{
    ennemi = p_ennemi;
}

void Narration::GameCore()
{
    int endGame = 0;
    int fileNbr = 1;
    std::string path = "../text_test/";
    std::string fullpath = "";
    std::string ligne;
    std::string confirm;
    int nbrPath = 0;
    std::string paths;

    while(not endGame)
    {
        fullpath = path + std::to_string(fileNbr) + ".txt";
        std::ifstream fichier { fullpath }; 

        getline(fichier, ligne);
        paths = ligne;
        nbrPath = wordCount(ligne);
        int requirement = 0;
        int combat = 0;

        while (getline(fichier, ligne))
        {
            std::istringstream iss { ligne };
            std::vector<std::string> mots;
            std::string mot;
            while (getline(iss, mot, ' '))
            {
                mots.push_back(mot);
            }
            
            if (mots[0] == "FF")
            {
                getline(fichier, ligne);
                std::cout << ligne << std::endl << std::endl;

                if (combat == 1)
                {
                    std::cout << "vous entrez en combat" << std::endl;
                    ennemi->infos();
                    
                    std::cout << "Pour commencer le combat equipe une arme " << std::endl;
                    std::cout << "Taper le nom d une des armes suivantes :" << std::endl;

                    heros->afficherEquipement();

                    std::string armeChoisi;
                    cin >> armeChoisi;

                    if (armeChoisi == "epee")
                    {
                        while (ennemi->getPuissance() > 0)
                        {
                            std::cout << "Pour attaquer taper 1:" << std::endl;
                            int choix;
                            std::cin >> choix;
                            if (choix == 1)
                            {
                                std::vector<Equipement> currentEquipement = heros->getEquipement();
                                int attaque = currentEquipement[0].rollAttaque();
                                std::cout << attaque << std::endl;

                                ennemi->setPuissance(ennemi->getPuissance() - attaque);
                            }
                        }
                        std::cout << "L'ennemi est mort" << std::endl;
                        requirement = 1;
                    }
                }
            }
            else if (mots[0] == "C")
            {
                combat = 1;
                Ennemi * en = new Ennemi();
                this->setEnnemi(en);
                ennemi->setPuissance(stoi(mots[1]));
            }

            else if (mots[0] == "D")
            {
                combat = 1;
                ennemi->setDiscussion(stoi(mots[1]));
            }

            else if (mots[0] == "OBJ")
            {
                std::cout << "Voyageur vous recevez un objet : " << mots[1] << mots[2] << std::endl << std::endl;
                heros->AddObjet(stoi(mots[2]));
            }

            else if (mots[0] == "UOBJ")
            {
                std::cout << "Voyageur, pour continuer vous devez posseder l'objet " << mots[1] << std::endl;
                
                if(heros->CheckObjet(stoi(mots[1])))
                {
                    std::cout << "Vous avez bien le bon objet" << std::endl;
                    requirement = 1;
                }
                else
                {
                    std::cout << "Vous n'avez pas le bon objet" << std::endl;
                }
                
            }

            else if (mots[0] == "FF1" and requirement == 0)
            {
                break;
            }

            else
            {
                std::cout << ligne << std::endl << std::endl;
            }
            

        } 
        requirement = 0;


        if (nbrPath > 1)
        {
            std::cout << "plusieurs chemin" << std::endl;

            std::vector<int> chemins;
            std::istringstream iss {paths};
            std::string mot;
            int choosedPath;
            int correctPath = 0;

            while (correctPath == 0)
            {
                while (getline(iss, mot,' '))
                {
                    std::cout << mot << std::endl;
                    chemins.push_back(stoi(mot));
                }
                std::cin >> choosedPath;

                while (!cin.good())
                {
                    std::cout << "Chemin choisi incorrect" << std::endl;
                    cin.clear();
                    cin.ignore(INT_MAX, '\n');
                    std::cin >> choosedPath;
                }
                if(std::count(chemins.begin(), chemins.end(), choosedPath))
                {
                    fileNbr = choosedPath;
                    correctPath = 1;
                }
                else
                {
                    std::cout << "Chemin choisi incorrect" << std::endl;
                    iss.clear();
                    iss.seekg(0);
                }
            }
            


        }

        else
        {
            fileNbr = stoi(paths);
        }
        

        std::cout << "Press a key to continue" << std::endl;
        std::cin >> confirm;

    }
}